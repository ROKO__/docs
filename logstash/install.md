Installation:

[Elastic  Official Docs](https://www.elastic.co/guide/en/beats/filebeat/current/setup-repositories.html)
[HowtoForge Tutorial](https://www.howtoforge.com/how-to-install-elastic-stack-on-centos-8/)

**_I've followed the HowtoForge's tutorial as it's perefect compared to official one._**
- **_First of all it's required to install java._**

 `sudo dnf install java-1.8.0-openjdk`

- Import the Elasticsearch PGP Key
  `sudo rpm ––import https://artifacts.elastic.co/GPG-KEY-elasticsearch`
  `cd /etc/yum.repos.d/` `sudo nano elasticsearch.repo`

* Type or copy the following lines:

```
[elasticstack]
name=Elastic repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```

`sudo dnf update ; sudo dnf install logstash`

- [logstash.yml](./logstash.yml) is configuration file of logstash, all
  necessary configurations are http.host http.port log.level and path.log.

- /etc/logstash/jvm.options and heap tunning. Min Heap `-Xms1536m` Max Heap
  `-Xmx1536m` **_Maximum recommended by Elastic is up to 50% of available RAM,
  otherwise you are about to run in many troubles (crashes mostly)._**

* After installation start logstash service
  `sudo systemctl enable --now logstash.service`

**Warning!!! _Be aware_ of yaml syntax indentation!!! You can use yamllint to
check configuration files after any changes.**
