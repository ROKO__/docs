Installation:

[Elastic  Official Docs](https://www.elastic.co/guide/en/beats/filebeat/current/setup-repositories.html)
[HowtoForge Tutorial](https://www.howtoforge.com/how-to-install-elastic-stack-on-centos-8/)

**_I've followed the HowtoForge's tutorial as it's perefect compared to official one._**

- Import the Elasticsearch PGP Key
  `sudo rpm ––import https://artifacts.elastic.co/GPG-KEY-elasticsearch`
  `cd /etc/yum.repos.d/` `sudo nano elasticsearch.repo`

* Type or copy the following lines:

```
[elasticstack]
name=Elastic repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```

`sudo dnf update ; sudo dnf install filebeat`

- [filebeat.yml](./filebeat.yml) is configuration file of filebeat, this is my
  working configuration from roi-app, it's same everywhere on client side.

* After installation use the attached filebeat.yml and start filebeat service
  `sudo systemctl enable --now filebeat.service`

**Warning!!! _Be aware_ of yaml syntax indentation!!! You can use yamllint to
check configuration files after any changes.**
