The following common issues could happen sometimes, i'll provide some tips
of finding and fixing them.

- For example you have a working ELK installation (filebeat test config shows
  OK, but anyway it's not fine due to yaml indentation), but anyway it doesn't
  show/parse any logs in kibana webui.

  1. The first think it's to check all yaml configuration files indentation with
     yamllint, and to correct the offending lines and restart filebeat service
     by: `sudo systemctl restart filebeat.service`

  2. If issue still persist, you can check the connection between filebeat and
     logstash/elasticsearch by: `filebeat test output`

  3. You have to check and connection between logstash and elasticsearch as
     well, but there's no built in option like in filebeat, so you'll need to
     check logstash logs `tail -f /var/log/logstash/logstash-plain.log`

  4. Check if all services are running. `systemctl status filebeat` on the
     client side, `systemctl status elasticsearch logstash kibana` (If it's a
     cluster installation run them separate. For example on all elastic nodes
     and all logstash nodes).

  5. Check for filters mistakes in logstash log
     `tail -f /var/log/logstash/logstash-plain.log`

**It not happens often but most of failures are caused by 95%+ disk usage, and out of ram situations.**

**In that case it's necessary to open SSH session on main node (192.168.56.202), and execute the following command (which is also in crontab /etc/cron.daily/elastic-curator), which will close and remove older than 40 days indicies**

**_At the moment with 300GB of disk space the absolute maximum is 40 days!_**

`curator_cli --http_auth elastic:2RnIt2Orv1POOzsaS2NCnsoU --host 192.168.56.202 delete_indices --filter_list '{"filtertype":"age","source":"name","direction": "older","timestring":"%Y.%m.%d","unit":"days","unit_count":40}'`

**In case of running out of ram, all we can do is to raise up the server RAM (if possible) or to reduce JVM heap size of elasticsearch and logstash, path for both configurations /etc/logstash/jvm.options and /etc/elasticsearch/jvm.options Xms and Xmx**

Some useful links for debugging. **_Use credentials provided in 1pass for
kbn.perspecta.org_**

[cluster state](http://192.168.56.202:9200/_cluster/state?pretty)
[cluster health](http://192.168.56.202:9200/_cluster/health?pretty)
[cluster indicies](http://192.168.56.202:9200/_cat/indices?v)
[cluster nodes](http://192.168.56.202:9200/_cat/nodes)

Also available as console tools in master node (192.168.56.202) of elasticsearch
located in /usr/local/bin
