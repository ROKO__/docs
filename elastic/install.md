Installation:

[Elastic  Official Docs](https://www.elastic.co/guide/en/beats/filebeat/current/setup-repositories.html)
[HowtoForge Tutorial](https://www.howtoforge.com/how-to-install-elastic-stack-on-centos-8/)

**_I've followed the HowtoForge's tutorial as it's perefect compared to official one._**
- **_First of all it's required to install java._**

 `sudo dnf install java-1.8.0-openjdk`

- Import the Elasticsearch PGP Key
  `sudo rpm ––import https://artifacts.elastic.co/GPG-KEY-elasticsearch`
  `cd /etc/yum.repos.d/` `sudo nano elasticsearch.repo`

* Type or copy the following lines:

```
[elasticstack]
name=Elastic repository for 7.x packages
baseurl=https://artifacts.elastic.co/packages/7.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```

`sudo dnf update ; sudo dnf install elasticsearch`

- [elasticsearch.yml](./elasticsearch.yml) is configuration file of
  elasticsearch, this is my working configuration from elk cluster, it's same
  for all cluster nodes.

- /etc/logstash/jvm.options and heap tunning. Min Heap `-Xms1536m` Max Heap
  `-Xmx1536m` **_Maximum recommended by Elastic is up to 50% of available RAM,
  otherwise you are about to run in many troubles (crashes mostly)_**

- After installation use the attached elasticsearch.yml and start elasticsearch
  service `sudo systemctl enable --now elasticsearch.service`

Note: My configuration is heavy custimized (cluster) compared to default one,
but all the options existing in stock file, i've just removed commented lines,
and has wroten my own comments. For a basic installation you would not need all
the stuff, but just network.host and network.port

If you're about to install a cluster structure, you'll need my configuration
which is ready for use.

**Warning!!! _Be aware_ of yaml syntax indentation!!! You can use yamllint to
check configuration files after any changes.**
