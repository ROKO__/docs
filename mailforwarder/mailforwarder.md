# Postfix + Anonaddy setup on Ubuntu Server<br><br>

### References<br>

- [SelfHosting Official Documentation](https://github.com/anonaddy/anonaddy/blob/master/SELF-HOSTING.md)<br>
- [dkim/spf](https://www.linuxbabe.com/mail-server/setting-up-dkim-and-spf)
- [redis](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04)

### **_For the installation and updating instructions please check the links in References._**<br>

## Postfix + Anonaddy Configuration<br>

#### DNS Records<br>
> mailforwarder		A       94.155.239.101<br><br>

> MX @ mail.example.com<br><br>

> mailforwarder		TXT	"v=spf1 mx ip4:94.155.239.101 ~all"<br><br>

> default._domainkey.mailforwarder.perspecta.org.		IN	TXT	( "v=DKIM1; h=sha256; k=rsa; "
	  "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA96Pwlykh7tTrBndhG5S2In1sSyHzzNJqdKZGEub+/6rH3dI0Qw+q1hqoqoXb9Yx6kEWEc1Qfe263ekVOmlZ1hALa/+W+dy6xa4oYrAX7evSc/F+ILLgeydAa3UKrOTSqpPO8aTs9dZgThd5kHE7tz4jmhtfLffLfbd6kdheh9ONSJY9icsInmP5oOEbJAo91x/efSbTm4STZ1p"
	  "zhkrMhtfBUiCwmkOZ5JBDWSVwTQmvJxM/AICk+3sLEhNeUlLpMEs2AUk/8/I0B8JHpq9wCucSL3IPFdTSUih/P8k/6zbQg7RPM94Kgs9s3B+RIICKsWGNTgLjoG46JNuZItxcyYwIDAQAB" )  ; ----- DKIM key default for mailforwarder.perspecta.org<br><br>

  >  _dmarc.mailforwarder.perspecta.org.	TXT	"v=DMARC1; p=none"<br><br>

#### Anonaddy custom domains DNS Records<br>
> getmefollowed.com   A   94.155.239.101<br><br>

> default._domainkey    CNAME   default._domainkey.perspecta.org<br><br>

#### Configuration files (I'll mention only files edited for anonaddy setup).<br>
> /etc/postfix/main.cf<br><br>

> /etc/postfix/master.cf<br><br>

> /etc/postfix/mysql-recipient-access.cf<br><br>

> /etc/postfix/mysql-recipient-access-domains-and-additional-usernames.cf<br><br>

> /etc/postfix/mysql-virtual-alias-domains-and-subdomains.cf<br><br>

> /var/www/anonaddy/.env<br><br>

> /etc/redis/redis.conf<br><br>

> /etc/supervisor/conf.d/anonaddy.conf<br><br>
